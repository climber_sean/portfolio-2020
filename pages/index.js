import css from '../styles/style.less';
import Header from '../components/header';
import Hero from '../components/hero';
import Head from 'next/head';

const Index = () => (
    <div>
        <Head>
            <link href="https://fonts.googleapis.com/css?family=Lato&amp;display=swap" rel="stylesheet" />
            <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&amp;display=swap" rel="stylesheet"></link>
        </Head>
        <Header />
        <Hero />
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
        <p>Filler</p>
    </div>
);

export default Index;