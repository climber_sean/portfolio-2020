import Link from 'next/link';

const Header = () => (
    <header>
        <div className="header">
            <div className="header__left">
                SB
            </div>
            <div className="header__right">
                <Link href="#">
                    <a>Home</a>
                </Link>
                <Link href="#">
                    <a>About</a>
                </Link>
                <Link href="#">
                    <a>Work</a>
                </Link>
                <Link href="#">
                    <a>Contact</a>
                </Link>
            </div>
        </div>
    </header>
);

export default Header;