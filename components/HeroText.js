import React from 'react';
import { motion, useMotionValue, useViewportScroll, useTransform } from 'framer-motion';

const HeroMotion = (props) => {
        let x = useMotionValue(0);
        x.set(props.btnOffset);
        let btOffset = props.offset;
        let y = useTransform(x, [0, btOffset], [1, 0]);
        let yy = useTransform(x, [0, btOffset], [0, -35])
        console.log(btOffset);

        return <motion.div style={{ opacity: y, y: yy}}>
        <h1>Hi,<br />I'm Sean Butlin</h1>
        </motion.div>
}

export default class HeroText extends React.Component {

    state = {
        currentScroll: 0,
        offsetScroll: 0
    }

    componentDidMount() {
        const btn = document.querySelector('.btn');
        this.setState({ offsetScroll: btn.offsetTop})
        // this.setState({currentScroll: btnOffset});
        window.addEventListener('scroll', this.checkScr);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll');
    }

    checkScr = e => {
        // console.log(Math.floor(this.state.currentScroll));
        let scroll = e.target.defaultView.scrollY;
        this.setState({ currentScroll: scroll });
    }
    
    

    render() { 
        return <HeroMotion btnOffset={this.state.currentScroll} offset={this.state.offsetScroll} onSroll={this.checkScr} />
    }
};