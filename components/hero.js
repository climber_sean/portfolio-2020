import Link from 'next/link';
import React from 'react';
import HeroText from '../components/HeroText'

const Hero = () => (
    <div className="hero">
        <div className="container" id="hero-container">
            <HeroText />
            <Link href="#">
                <a className="btn">View Work</a>            
            </Link>
        </div>
    </div>
);

export default Hero;